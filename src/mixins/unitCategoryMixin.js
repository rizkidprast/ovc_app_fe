export default {
  data () {
    return {
      units: [],
      categories: []
    }
  },
  created () {
  },
  mounted () {
    this.fetchUnits()
    this.fetchCategories()
    // console.log(this.getUnitName)
  },
  methods: {
    async fetchUnits () {
      try {
        var res = await this.$api.units.get()
        this.units = res.data
      } catch (e) {
        this.$toastr.error(this.$util.err(e))
      }
    },
    async fetchCategories () {
      try {
        var res = await this.$api.categories.get()
        this.categories = res.data
      } catch (e) {
        this.$toastr.error(this.$util.err(e))
      }
    },
    getUnitName (id) {
      let model = this.units.find(x => x.id === id)
      if (!model) return ''
      return model.code
    },
    getCategoryName (id) {
      let model = this.categories.find(x => x.id === id)
      // console.log('ca', id, model)
      if (!model) return ''
      return model.name
    }
  }
}
