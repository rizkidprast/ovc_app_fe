import { SelectViewModel } from '../models'
import { extend, date, format, copyToClipboard } from 'quasar'

const {
  formatDate,
  getDateDiff,
  extractDate,
  subtractFromDate,
  addToDate,
  isSameDate,
  isValid
} = date
const { capitalize, humanStorageSize } = format

export const utility = {
  // quasar utilities
  extend,
  formatDate,
  getDateDiff,
  extractDate,
  subtractFromDate,
  addToDate,
  isValid,
  isSameDate,
  capitalize,
  humanStorageSize,
  copyToClipboard,

  toNumber (val) {
    if (!val) return 0

    if (val.replace) {
      let ns = val.replace(/\./g, '').replace(/,/, '.'), n = Number(ns)
      if (Number.isNaN(n)) return 0
      return n
    }

    return Number(val)
  },
  getSexTypes () {
    return [
      new SelectViewModel('F', 'Female'),
      new SelectViewModel('M', 'Male'),
      new SelectViewModel('', 0, true)
    ]
  },
  safeString (model) {
    if (typeof model !== 'string') { return '' }
    return model.toString().replace(/</g, '&lt;').replace(/>/g, '&gt;')
  },
  // get error message
  err (e, outputArray) {
    // console.log('err util')
    outputArray = outputArray || false
    let msg = outputArray ? [] : ''
    if (e.response) {
      console.log('errresponse', e.response)
      if (e.response.data && e.response.data.modelState) {
        let ms = e.response.data.modelState
        var errs = this.errModelState(ms)
        for (var err of errs) {
          if (outputArray) {
            msg.push(err[1])
          } else {
            msg = msg + ';' + err[1]
          }
        }

        if (!outputArray && msg.indexOf(';') === 0) {
          msg = msg.substr(1)
        }
      } else if (e.response && e.response.data.message) {
        if (outputArray) {
          msg.push(e.response.data.message)
        } else {
          msg = e.response.data.message
        }
      } else {
        var l = e.response.data.toString().length
        if (outputArray) {
          msg.push(
            l < 100 && l > 0
              ? e.response.data
              : e.response.statusText
          )
        } else {
          msg =
          l < 100 && l > 0
            ? e.response.data
            : e.response.statusText
        }
      }
    } else if (e.request) {
      // The request was made but no response was received
      // `e.request` is an instance of XMLHttpRequest in the browser and an instance of
      // http.ClientRequest in node.js
      // console.log(e.request);

      if (outputArray) {
        msg.push(e.request.data.message)
      } else {
        msg = e.request.data.message
      }
    } else if (e.modelState) {
      let ms = e.modelState
      let errs = this.errModelState(ms)
      for (let err of errs) {
        if (outputArray) {
          msg.push(err[1])
        } else {
          msg = msg + ';' + err[1]
        }
      }

      if (!outputArray && msg.indexOf(';') === 0) {
        msg = msg.substr(1)
      }
    } else if (e.message) {
      if (outputArray) {
        msg.push(e.message)
      } else {
        msg = e.message
      }
    } else {
      if (outputArray) {
        msg.push(
          e.toString().length < 100
            ? e
            : 'Error occured while contacting to server'
        )
      } else {
        msg =
          e.toString().length < 100
            ? e
            : 'Error occured while contacting to server'
      }
    }

    return msg
  },
  errArray (e) {
    return this.err(e, true)
  },
  errModelState (modelState) {
    var arr = []
    if (typeof modelState === 'object') {
      for (var k in modelState) {
        if (!Array.isArray(modelState[k])) break
        arr.push([k, modelState[k][0]])
      }
    }
    return arr
  }
}

export default async ({ Vue }) => {
  Vue.prototype.$util = utility
}
