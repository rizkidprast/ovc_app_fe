import money from 'v-money'

export { VMoney } from 'v-money'
export const configs = {
  precision: 2,
  decimal: ',',
  thousands: '.',
  prefix: '',
  masked: false
}

export default async ({ Vue }) => {
  Vue.use(money, configs)
}
