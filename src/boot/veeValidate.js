import id from 'vee-validate/dist/locale/id'
import VeeValidate, { Validator } from 'vee-validate'
import { api } from './api'
import { utility } from './utility'

export default async ({ Vue }) => {
  Vue.use(VeeValidate)

  Validator.localize('id', id)

  const remoteValUserCode = {
    getMessage (field, params, data) {
      return (data && data.message) || 'Something went wrong'
    },
    validate (v1, v2) {
      return new Promise(async (resolve, reject) => {
        if (Array.isArray(v2) && v2.length > 0) {
          v2 = v2[0]
        }
        var res = await api.clients.validateUserCode(v1, v2)
        if (res.status > 300) {
          resolve({ valid: false, data: { message: utility.err(res) } })
        }

        if (typeof (res.data) === 'string') {
          resolve({ valid: false, data: { message: res.data } })
        } else {
          resolve({ valid: res.data, data: { message: '' } })
        }
      })
    }
  }

  const remoteValSku = {
    getMessage (field, params, data) {
      return (data && data.message) || 'Something went wrong'
    },
    validate (v1, v2) {
      return new Promise(async (resolve, reject) => {
        if (Array.isArray(v2) && v2.length > 0) {
          v2 = v2[0]
        }
        var res = await api.products.validateSku(v1, v2)
        if (res.status > 300) {
          resolve({ valid: false, data: { message: utility.err(res) } })
        }

        if (typeof (res.data) === 'string') {
          resolve({ valid: false, data: { message: res.data } })
        } else {
          resolve({ valid: res.data, data: { message: '' } })
        }
      })
    }
  }

  Validator.extend('remoteValClientCode', remoteValUserCode, { hasTarget: true, immediate: false })
  Validator.extend('remoteValSku', remoteValSku, { hasTarget: true, immediate: false })
}
