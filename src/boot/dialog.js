import AppointmentCalendar from '../components/appointments/AppointmentCalendar'
import AppointmentsTable from '../components/appointments/AppointmentsTable'
import AppointmentsTableForCustomer from '../components/appointments/AppointmentsTableForCustomer'
import AppointmentDetails from '../components/appointments/AppointmentDetails.vue'

import { Dialog } from 'quasar'

let dialog = function (app, store, Vue) {
  function openAppointmentCalendar () {
    Dialog.create({
      component: AppointmentCalendar,
      parent: app,
      // $store: this.$store
      $store: app.store
    })
      .onOk(() => {
        // console.log('ok');
      })
      .onCancel(() => {
        // console.log('Cancel');
      })
      .onDismiss(() => {
        // console.log('dismiss');
      })
  }

  function openAppointmentsTable () {
    Dialog.create({
      component: AppointmentsTable
      // parent: $vue
      // $store: app.store
    })
      .onOk(() => {
        // console.log('ok');
      })
      .onCancel(() => {
        // console.log('Cancel');
      })
      .onDismiss(() => {
        // console.log('dismiss');
      })
  }

  function openAppointmentDetails ($vue, id, customerVarId) {
    store.commit('setAppointmentId', id || 0)
    Dialog.create({
      component: AppointmentDetails,
      parent: $vue,
      customerVarId: customerVarId
      // $store: this.$store
    })
      .onOk(() => {
        // console.log('ok');
      })
      .onCancel(() => {
        // console.log('Cancel');
      })
      .onDismiss(() => {
        // console.log('dismiss');
      })
  }

  function openAppointmentsTableForCustomer ($vue, clientId, title) {
    Dialog.create({
      component: AppointmentsTableForCustomer,
      parent: $vue,
      clientId,
      title
      // $store: app.store
    })
      .onOk(() => {
        // console.log('ok');
      })
      .onCancel(() => {
        // console.log('Cancel');
      })
      .onDismiss(() => {
        // console.log('dismiss');
      })
  }

  return {
    calendar: openAppointmentCalendar,
    appointmentTable: openAppointmentsTable,
    appointmentDetails: openAppointmentDetails,
    appointmentTableForCustomer: openAppointmentsTableForCustomer
  }
}

export default async ({ app, store, Vue }) => {
  // console.log('app', app)
  Vue.prototype.$dialog = dialog(app, store, Vue)
}
