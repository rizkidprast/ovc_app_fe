import { Notify } from 'quasar'

export default async ({ Vue }) => {
  Notify.setDefaults({
    position: 'bottom-right',
    timeout: 2500,
    textColor: 'white',
    actions: [{ icon: 'fa fa-times', size: 'xs', color: 'white' }]
  })

  Vue.prototype.$toastr = {
    error (msg) {
      Notify.create(
        {
          color: 'red-5',
          textColor: 'white',
          icon: 'fas fa-exclamation-triangle',
          message: `${msg}`
        }
      )
    },
    success (msg) {
      Notify.create({
        color: 'green-4',
        textColor: 'white',
        icon: 'fas fa-check-circle',
        message: msg
      })
    },
    warning (msg) {
      Notify.create({
        color: 'yellow-4',
        textColor: 'primary',
        icon: 'fas fa-check-circle',
        message: msg
      })
    }
  }
}
