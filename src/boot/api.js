import { instance as axios } from '../boot/axios'
import store from '../store'
import { Category, Unit, AnimalType } from 'src/models'
import { utility } from '../boot/utility'

const response = (obj) => {
  return new Promise((resolve) => {
    resolve({
      status: 200,
      data: obj
    })
  })
}

export const api = {
  auth: {
    get: () => axios.get('/auth'),
    login: (username, password) => axios.post('/auth/login', {
      username,
      password
    }),
    logout: () => axios.post('/auth/logout')
  },
  animalTypes: {
    get: async () => {
      const animals = store.state.list.animalTypes.length
      if (animals.length) {
        return response(animals)
      }
      let res = await axios.get('/animalTypes')
      if (res.status < 300) {
        store.commit('list/setAnimalTypes', res.data)
      }
      return res
    },
    getOne: (id) => axios.get(`/animalTypes/${id}`),
    post: async (params) => {
      let res = await axios.post('/animalTypes', { ...params })
      if (res.status < 300) {
        var data = store.state.list.animalTypes.map(x => x)
        data.push(new AnimalType(params))
        store.commit('list/setAnimalTypes', data)
      }
      return res
    },
    put: async (id, params) => {
      let res = await axios.put(`/animalTypes/${id}`, { ...params })
      if (res.status < 300) {
        var data = store.state.list.animalTypes.map(x => x.id === id ? new AnimalType(params) : x)
        store.commit('list/setAnimalTypes', data)
      }
      return res
    },
    delete: async (id) => {
      let res = await axios.delete(`/animalTypes/${id}`)
      if (res.status < 300) {
        var data = store.state.list.animalTypes.filter(x => x.id !== id)
        store.commit('list/setAnimalTypes', data)
      }
      return res
    }
  },
  categories: {
    get: async () => {
      const model = store.state.list.categories.length
      if (model.length) {
        return response(model)
      }
      let res = await axios.get('/categories')
      if (res.status < 300) {
        store.commit('list/setCategories', res.data)
      }
      return res
    },
    getOne: (id) => axios.get(`/categories/${id}`),
    post: async (params) => {
      let res = await axios.post('/categories', { ...params })
      if (res.status < 300) {
        var data = store.state.list.animalTypes.map(x => x)
        data.push(new Category(params))
        store.commit('list/setCategories', data)
      }
      return res
    },
    put: async (id, params) => {
      let res = await axios.put(`/categories/${id}`, { ...params })
      if (res.status < 300) {
        var data = store.state.list.animalTypes.map(x => x.id === id ? new Category(params) : x)
        store.commit('list/setCategories', data)
      }
      return res
    },
    delete: async (id) => {
      let res = await axios.delete(`/categories/${id}`)
      if (res.status < 300) {
        var data = store.state.list.animalTypes.filter(x => x.id !== id)
        store.commit('list/setCategories', data)
      }
      return res
    }
  },
  units: {
    get: async () => {
      const model = store.state.list.categories.length
      if (model.length) {
        return response(model)
      }
      let res = await axios.get('/units')
      if (res.status < 300) {
        store.commit('list/setUnits', res.data)
      }
      return res
    },
    getOne: (id) => axios.get(`/units/${id}`),
    post: async (params) => {
      let res = await axios.post('/units', { ...params })
      if (res.status < 300) {
        var data = store.state.list.animalTypes.map(x => x)
        data.push(new Unit(params))
        store.commit('list/setUnits', data)
      }
      return res
    },
    put: async (id, params) => {
      let res = await axios.put(`/units/${id}`, { ...params })
      if (res.status < 300) {
        var data = store.state.list.animalTypes.map(x => x.id === id ? new Unit(params) : x)
        store.commit('list/setUnits', data)
      }
      return res
    },
    delete: async (id) => {
      let res = await axios.delete(`/units/${id}`)
      if (res.status < 300) {
        var data = store.state.list.animalTypes.filter(x => x.id !== id)
        store.commit('list/setUnits', data)
      }
      return res
    }
  },
  userTypes: {
    get: async () => {
      return response(store.state.list.userTypes)
    }
  },
  appointments: {
    get: (info) => axios.get('Appointments/CalendarEvents', {
      params: {
        start: info.startStr,
        end: info.endStr
      }
    }),
    getOne: (id) => axios.get(`/Appointments/${id}`),
    getByClient: (clientId) => axios.get('/Appointments/ByClient', { params: { clientId } }),
    post: (params) => axios.post('/Appointments', { ...params }),
    put: (id, params) => axios.put(`/Appointments/${id}`, { ...params }),
    delete: (id) => axios.delete(`/Appointments/${id}`),
    getCount: () => axios.get('/Appointments/GetCount'),
    getAtMonth: (date) => axios.get('/Appointments/AtMonth', { params: { date } }),
    getAtDate: (date) => axios.get('/Appointments/AtDate', { params: { date } })

  },
  selectDatas: {
    clients: (params) => axios.get('selectdata/Clients', { params }),
    vets: () => axios.get('selectdata/users', { params: { userTypeId: 1 } }),
    users: (params) => axios.get('selectdata/users', { params }),
    products: (params) => axios.get('selectdata/products', { params }),
    insentiveModels: async () => {
      var data = store.state.list.insentiveModels
      if (!data.length) {
        let res = await axios.get(`/selectdata/InsentiveModels`)
        if (res.status < 300) {
          data = res.data
          store.commit('list/setInsentiveModels', res.data)
        }
      }
      return response(data)
    }
  },
  clients: {
    get: (params) => axios.get('/clients', { params }),
    getOne: (id) => axios.get(`/clients/${id}`),
    post: (params) => axios.post('/clients', { ...params }),
    put: (id, params) => axios.put(`/clients/${id}`, { ...params }),
    delete: (id) => axios.delete(`/clients/${id}`),
    validateUserCode: (v1, v2) => {
      return axios.get('clients/validateClientCode', { params: { code: v1, id: (v2 || 0) } })
    },
    getInventoryUseForMedical: (clientId, params) => axios.get('/InventoryUseForMedicals', { params: { clientId, ...params } }),
    postInventoryUseForMedical: (params) => axios.post('/InventoryUseForMedicals', params)
  },
  patients: {
    get: (params) => axios.get('/patients/byclient', { params }),
    getOne: (id) => axios.get(`/patients/${id}`),
    post: (params) => axios.post('/patients', { ...params }),
    put: (id, params) => axios.put(`/patients/${id}`, { ...params }),
    delete: (id) => axios.delete(`/patients/${id}`),
    getSelectDataByClient: async (clientId) => {
      var data = store.state.list.patientsByClient[clientId]
      if (!data || !data.length) {
        let res = await axios.get(`/patients/byclient`, { params: { clientId } })
        if (res.status < 300) {
          data = res.data
          store.commit('list/setPatientsByClient', res.data)
        }
      }
      return response(data)
    }
  },
  signalements: {
    get: (params) => axios.get('/signalements/bypatient', { params }),
    getOne: (id) => axios.get(`/signalements/${id}`),
    post: (params) => axios.post('/signalements', { ...params }),
    put: (id, params) => axios.put(`/signalements/${id}`, { ...params }),
    delete: (id) => axios.delete(`/signalements/${id}`),
    uploadUrl: (id) => `/api/signalements/${id}/Upload`,
    clearAttachment: (id) => axios.put(`/signalements/${id}/clearAttachment`)
  },
  products: {
    get: (discontinued, params) => axios.get('/products', { params: {
      discontinued,
      ...params
    } }),
    getOne: (id) => axios.get(`/products/${id}`),
    post: (params) => axios.post('/products', { ...params }),
    put: (id, params) => axios.put(`/products/${id}`, { ...params }),
    delete: (id) => axios.delete(`/products/${id}`),
    validateSku: (v1, v2) => {
      return axios.get('products/validateSku', { params: { sku: v1, id: (v2 || 0) } })
    }
  },

  sales: {
    get: (params) => axios.get('/sales', { params }),
    getToday: () => {
      let res = axios.get('/sales/today')
      return res
    },
    post: (params) => axios.post('/sales', { ...params }),
    closing: (params) => axios.post('/salesmanagers', { ...params }),
    // getOne: (id) => axios.get(`/units/${id}`),
    put: (id, params) => axios.put(`/sales/${id}`, { ...params })
    // delete: (id) => axios.delete(`/units/${id}`)
  },
  salesLines: {
    getBySales: (salesId) => axios.get('/saleslines', { params: { salesId } })
    // getOne: (id) => axios.get(`/units/${id}`),
    // put: (id, params) => axios.put(`/units/${id}`, { ...params }),
    // delete: (id) => axios.delete(`/units/${id}`)
  },
  stocks: {
    get: (params) => axios.get('stockJournals', { params }),
    postJournal: (params) => axios.post('/stockJournals', params)
  },
  onSites: {
    get: () => axios.get('onSites'),
    postByClient: (clientId) => axios.post('/onsites/byClient', clientId),
    postByPatients: (patientIds) => axios.post('/onsites/byPatients', patientIds),
    setBoarding: (patientId) => axios.post('/onsites/setBoarding', patientId),
    delete: (id) => axios.delete(`/onsites/${id}`)
  },
  purchases: {
    get: () => axios.get('/purchases'),
    getOne: (id) => axios.get(`/purchases/${id}`),
    post: (params) => axios.post('/purchases', { ...params }),
    put: (id, params) => axios.put(`/purchases/${id}`, { ...params }),
    delete: (id) => axios.delete(`/purchases/${id}`)
  },
  insentiveModels: {
    get: () => axios.get('/insentiveModels'),
    getOne: (id) => axios.get(`/insentiveModels/${id}`),
    post: (params) => axios.post('/insentiveModels', { ...params }),
    put: (id, params) => axios.put(`/insentiveModels/${id}`, { ...params }),
    delete: (id) => axios.delete(`/insentiveModels/${id}`)
  },
  deposits: {
    get: (params) => axios.get('/depositTransactions', { params }),
    getToday: () => {
      let res = axios.get('/depositTransactions/today')
      return res
    },
    getTotalByClient: (clientId) => {
      let res = axios.get('/depositTransactions/byClient', { params: { clientId } })
      return res
    },
    getTotal: () => {
      let res = axios.get('/depositTransactions/TotalDeposit')
      return res
    },
    post: (params) => axios.post('/depositTransactions', { ...params })
  },
  dashboard: {
    getCardItems: () => axios.get('/dashboard/cardItems'),
    getSalesYears: () => axios.get('/dashboard/SalesYears'),
    getSalesOverview: () => axios.get('/dashboard/SalesOverview')
  },
  users: {
    get: (params) => axios.get('/users', { params }),
    getOne: (id) => axios.get(`/users/${id}`),
    post: (params) => axios.post('/users', { ...params }),
    put: (id, params) => axios.put(`/users/${id}`, { ...params }),
    delete: (id) => axios.delete(`/users/${id}`),
    changePassword: (params) => axios.put('/users/ChangePassword', { ...params })
  },
  roles: {
    get: (params) => axios.get('/roles', { params }),
    getOne: (id) => axios.get(`/roles/${id}`)
  },
  reports: {
    getInsentives: () => axios.get('/reports/insentive'),
    getSales: (date1, date2) => axios.get('/reports/sales', { params: { date1, date2 } }),
    getDailyRevenues: (params) => axios.get('/reports/revenuesummary', { params }),
    getDeposits: (date1, date2) => axios.get('/reports/deposit', { params: { date1, date2 } }),
    getProducts: (filter) => axios.get('/reports/product', { params: { filter } }),
    getProductHistories: (id) => axios.get('/reports/ProductHistory', { params: { id } }),

    downloadSalesUrl: (date) => `/api/reports/download/sales?date=${encodeURIComponent(utility.formatDate(date, 'YYYY/MM/DD'))}`,
    downloadInsentiveUrl: (date) => `/api/reports/download/insentive?date=${encodeURIComponent(utility.formatDate(date, 'YYYY/MM/DD'))}`,
    // downloadStockOpnameUrl: (date) => `/api/reports/download/stockopname?date=${encodeURIComponent(utility.formatDate(date, 'YYYY/MM/DD'))}`
    downloadStockOpnameUrl: () => `/api/reports/download/stockopname`
  }
}

export default async ({ Vue }) => {
  Vue.prototype.$api = api
}
