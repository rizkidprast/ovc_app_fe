import ValidationSummary from '../components/_injected/ValidationSummary'
import SubmitButton from '../components/_injected/SubmitButton'
import MainTable from '../components/_injected/MainTable'
import MainCard from '../components/_injected/MainCard'
import CustomerSelect from '../components/_injected/CustomerSelect'
// import CustomerSelectVarId from '../components/_injected/CustomerSelectVarId'
import ProductSelect from '../components/_injected/ProductSelect'
import DateInput from '../components/_injected/DateInput'
import KeyValueTable from '../components/_injected/KeyValueTable'
import UserSelect from '../components/_injected/UserSelect'
import MainTitleBanner from '../components/_injected/MainTitleBanner'
import MainLoader from '../components/_injected/MainLoader'
import MoneyField from '../components/_injected/MoneyField'
import QuantityField from '../components/_injected/QuantityField'
import { VMoney, configs } from '../boot/money'
import MyLoader from '../components/_injected/MyLoader'
import InsentiveTypeSelect from '../components/_injected/InsentiveTypeSelect'
import EdcTypeSelect from '../components/_injected/EdcTypeSelect'
import InsentiveModelSelect from '../components/_injected/InsentiveModelSelect'
import UserTypeSelect from '../components/_injected/UserTypeSelect'
import RoleSelect from '../components/_injected/RoleSelect'
import RolesSelect from '../components/_injected/RolesSelect'
import PatientSelect from '../components/_injected/PatientSelect'
import DateRangeInput from '../components/_injected/DateRangeInput'
import CategorySelect from '../components/_injected/CategorySelect'
import UnitSelect from '../components/_injected/UnitSelect'

// "async" is optional
export default async ({ Vue, app }) => {
  Vue.component('validation-summary', ValidationSummary)
  Vue.component('submit-button', SubmitButton)
  Vue.component('main-table', MainTable)
  Vue.component('main-card', MainCard)
  // Vue.component('customer-select-var-id', CustomerSelectVarId)
  Vue.component('customer-select', CustomerSelect)
  Vue.component('date-input', DateInput)
  Vue.component('key-value-table', KeyValueTable)
  Vue.component('user-select', UserSelect)
  Vue.component('main-title-banner', MainTitleBanner)
  Vue.component('main-loader', MainLoader)
  Vue.component('money-field', MoneyField)
  Vue.component('quantity-field', QuantityField)
  Vue.component('my-loader', MyLoader)
  Vue.component('product-select', ProductSelect)
  Vue.component('insentive-type-select', InsentiveTypeSelect)
  Vue.component('edc-type-select', EdcTypeSelect)
  Vue.component('user-type-select', UserTypeSelect)
  Vue.component('insentive-model-select', InsentiveModelSelect)
  Vue.component('role-select', RoleSelect)
  Vue.component('roles-select', RolesSelect)
  Vue.component('patient-select', PatientSelect)

  Vue.component('unit-select', UnitSelect)
  Vue.component('category-select', CategorySelect)
  Vue.component('date-range-input', DateRangeInput)

  Vue.directive('money', VMoney)

  app.moneyConfigs = configs

  Vue.filter('money', (val, configs) => {
    if (Number.isNaN(val)) {
      return val
    }
    var obj = Object.assign({}, { minimumFractionDigits: 2, maximumFractionDigits: 2, style: 'currency', currency: 'IDR', currencyDisplay: 'symbol' }, configs)
    return Number(val).toLocaleString('id-ID', obj)
  })

  Vue.mixin({
    computed: {
      auth () { return this.$store.state.auth },
      roles () { return this.$store.state.auth.roles },
      isAdmin () {
        return this.$store.getters['auth/isAdmin']
      },
      isFinance () {
        return this.$store.getters['auth/isFinance']
      },
      isVet () {
        return this.$store.getters['auth/isVet']
      }
    },
    methods: {

    }
  })
}
