import axios from 'axios'

export const instance = axios.create({
  baseURL: '/api',
  // timeout: 3000,
  headers: { Accept: 'application/json', 'Content-Type': 'application/json' }
})

export default async ({ Vue, store }) => {
  Vue.prototype.$axios = instance
}
