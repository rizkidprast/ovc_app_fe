import UserTracker from './UserTracker'

export class AppointmentType extends UserTracker {
  constructor (
    { id,
      typeName,
      codeColor, ...params } = {}
  ) {
    super(params)
    this.id = id
    this.typeName = typeName
    this.codeColor = codeColor
  }

  static defaultList () {
    var arr = []
    arr.push(new AppointmentType({ id: 1, typeName: 'Check up', codeBlock: 'green' }))
    arr.push(new AppointmentType({ id: 2, typeName: 'Vaccination', codeBlock: 'blue' }))
    arr.push(new AppointmentType({ id: 3, typeName: 'Surgery', codeBlock: '#8e83e6' }))
    arr.push(new AppointmentType({ id: 4, typeName: 'Grooming', codeBlock: 'yellow' }))
    arr.push(new AppointmentType({ id: 5, typeName: 'Boarding', codeBlock: 'purple' }))
    arr.push(new AppointmentType({ id: 1, typeName: 'Other', codeBlock: 'grey' }))
    return arr
  }
}
