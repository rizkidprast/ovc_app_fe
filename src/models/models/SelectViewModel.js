export class SelectViewModel {
  constructor (
    label,
    value,
    selected = false
  ) {
    this.label = label
    this.value = value
    this.selected = selected
  }
}
