export class User {
  constructor ({
    id,
    email,
    userTypeId,
    isActive = false,
    userName,
    displayName,

    userType,
    roles = [],
    roleIds = []
  } = {}) {
    this.id = id
    this.email = email
    this.userTypeId = userTypeId
    this.isActive = isActive
    this.userType = userType
    this.userName = userName
    this.displayName = displayName

    this.roleIds = roleIds
    this.roles = roles
  }

  get name () {
    return this.displayName && this.displayName !== null ? this.displayName : this.userName
  }
}
