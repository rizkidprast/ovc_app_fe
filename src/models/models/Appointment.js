import UserTracker from './UserTracker'

export class Appointment extends UserTracker {
  constructor (
    {
      id,
      appointmentTypeId,
      customerCode,
      name,
      phone,
      dueDate,
      forPurpose,
      isConfirmed,
      dueDateEnd,
      ...params
    } = {}
  ) {
    super(params)
    this.id = id
    this.appointmentTypeId = appointmentTypeId
    this.customerCode = customerCode
    this.name = name
    this.phone = phone
    this.dueDate = dueDate
    this.forPurpose = forPurpose
    this.isConfirmed = isConfirmed
    this.dueDateEnd = dueDateEnd
  }
}
