import UserTracker from './UserTracker'

export class DepositTransaction extends UserTracker {
  constructor ({
    id = 0,
    salesManagerId,
    depositCode,
    clientId,
    debit = 0,
    credit = 0,
    note,
    salesId,

    clientName,
    salesManager,
    ...params
  } = {}) {
    super(params)
    this.id = id
    this.salesManagerId = salesManagerId
    this.depositCode = depositCode
    this.clientId = clientId
    this.debit = debit
    this.credit = credit

    this.note = note
    this.salesId = salesId
    this.clientName = clientName
    this.salesManager = salesManager
  }
}
