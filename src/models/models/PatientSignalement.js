import UserTracker from './UserTracker'

export class PatientSignalement extends UserTracker {
  constructor (
    {
      id,
      patientId,
      visitDate = new Date(),
      weight,
      temp,
      resp,
      pulse,
      treatedBy,

      anamnesa,
      clinicalSign,
      treatment,
      diagnose,
      diffDiagnose,

      fileName,
      fileType,
      fileSize = 0,
      fileUri,

      ...params
    } = {}
  ) {
    super(params)
    this.id = id
    this.patientId = patientId
    this.visitDate = visitDate
    this.weight = weight
    this.temp = temp
    this.resp = resp
    this.pulse = pulse
    this.treatedBy = treatedBy

    this.anamnesa = anamnesa
    this.clinicalSign = clinicalSign
    this.treatment = treatment
    this.diagnose = diagnose
    this.diffDiagnose = diffDiagnose

    this.fileName = fileName
    this.fileType = fileType
    this.fileSize = fileSize
    this.fileUri = fileUri
  }

  static fromPatientId (patientId) {
    return new PatientSignalement({ patientId: patientId })
  }
}
