import UserTracker from './UserTracker'
import { Client } from './Client'

export class Sales extends UserTracker {
  constructor (
    {
      id,

      clientId,
      salesManagerId,
      salesDate,

      receiptCode,
      discountPercent = 0,
      discount = 0,
      tax = 0,
      cardSurcharge = 0,

      cash = 0,
      creditCard = 0,
      ccEdcType,
      debitCard = 0,
      dcEdcType,
      deposit = 0,

      isTracked,
      note,
      isPrinted,

      // calculated
      totalCost = 0,
      subTotal = 0,
      change = 0,
      // total = 0,
      isClosed,

      salesLines = [],

      client,
      salesManager,
      ...params
    } = {}
  ) {
    super(params)
    this.id = id
    this.clientId = clientId
    this.salesManagerId = salesManagerId

    this.salesDate = salesDate
    this.receiptCode = receiptCode
    this.discountPercent = discountPercent
    this.discount = discount
    this.subTotal = subTotal
    this.tax = tax
    this.cash = cash
    this.creditCard = creditCard
    this.ccEdcType = ccEdcType
    this.debitCard = debitCard
    this.dcEdcType = dcEdcType
    this.deposit = deposit

    this.isTracked = isTracked
    this.note = note
    this.isPrinted = isPrinted

    this.totalCost = totalCost
    // this.change = change
    // this.total = total

    this.cardSurcharge = cardSurcharge
    this.isClosed = isClosed

    this.salesLines = salesLines
    this.client = client && client !== null ? new Client(client) : undefined
    this.salesManager = salesManager
  }

  get total () {
    return this.subTotal - this.discount - (this.subTotal * this.discountPercent / 100)
  }

  get grandTotal () {
    return this.total + (this.total * this.tax / 100) + this.cardSurcharge
  }

  get totalPayment () {
    return (this.cash + this.creditCard + this.debitCard + this.deposit)
  }

  get revenue () {
    return this.totalPayment > this.grandTotal ? this.grandTotal : this.totalPayment
  }

  get discountPercentMoneyValue () {
    return this.subTotal * this.discountPercent / 100
  }

  get taxMoneyValue () {
    return this.tax * this.total / 100
  }

  get netCash () {
    return this.totalPayment > this.grandTotal ? this.cash - this.change : this.cash
  }

  get change () {
    return this.totalPayment - this.grandTotal
  }
}
