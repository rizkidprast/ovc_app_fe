import UserTracker from './UserTracker'
import { User } from './User'

export class SalesLineRelatedPic extends UserTracker {
  constructor (
    {
      id,
      salesLineId = 0,
      userTypeId = null,
      userId,

      user,
      ...params
    } = {}
  ) {
    super(params)
    this.id = id
    this.salesLineId = salesLineId
    this.userTypeId = userTypeId
    this.userId = userId
    this.user = user && user !== null ? new User(user) : undefined
  }
}
