import UserTracker from './UserTracker'
import { utility } from '../../boot/utility'

export class OnSite extends UserTracker {
  constructor (
    {
      id,
      clientId,
      patientId,
      startAt,
      client,
      patient,
      ...params
    } = {}
  ) {
    super(params)
    this.id = id
    this.clientId = clientId
    this.patientId = patientId
    this.startAt = startAt
    this.client = client
    this.patient = patient
  }

  get startAtString () {
    return this.startAt !== undefined ? utility.formatDate(this.startAt, 'dddd, DD MMM YYYY') : ''
  }

  get boardingDuration () {
    var d = new Date()
    return this.startAt !== undefined ? utility.getDateDiff(d, this.startAt, 'days') + ' hari' : ''
  }
}
