import { utility } from '../../boot/utility.js'
import { InsentiveModel } from './InsentiveModel.js'

export class Product {
  constructor ({ id = 0,
    name,
    sku,
    unitId,
    categoryId,
    purchasingPrice = 0,
    netPrice = 0,
    markup = 0,
    markupPercent = 0,
    discount = 0,
    discountPercent = 0,
    productionCost = 0,

    // sellingPrice = 0,

    trackInventory = false,
    minimumQty = 0,
    qty,
    discontinued = false,

    insentiveModelId,
    insentiveModel
  } = {}) {
    this.id = id
    this.name = name
    this.sku = sku
    this.unitId = unitId
    this.categoryId = categoryId
    this.purchasingPrice = purchasingPrice
    this.netPrice = netPrice
    this.markup = markup
    this.markupPercent = markupPercent
    this.discount = discount
    this.discountPercent = discountPercent
    this.productionCost = productionCost

    // this.sellingPrice = sellingPrice// generated

    this.trackInventory = trackInventory
    this.minimumQty = minimumQty
    this.qty = qty
    this.discontinued = discontinued

    this.insentiveModelId = insentiveModelId
    this.insentiveModel = insentiveModel && insentiveModel.name ? new InsentiveModel(insentiveModel) : undefined
  }

  get sellingPrice () {
    return utility.toNumber(this.netPrice) + utility.toNumber(this.markup) + (utility.toNumber(this.markupPercent) * utility.toNumber(this.netPrice) / 100) + utility.toNumber(this.productionCost) -
    utility.toNumber(this.discount) - (utility.toNumber(this.discountPercent) * utility.toNumber(this.netPrice) / 100)
  }
}
