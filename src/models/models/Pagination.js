export class Pagination {
  constructor (sortBy, { descending = false, page = 1, rowsPerPage = 15, rowsNumber = undefined } = {}) {
    this.sortBy = sortBy
    this.descending = descending
    this.page = page
    this.rowsPerPage = rowsPerPage
    this.rowsNumber = rowsNumber
  }
}
