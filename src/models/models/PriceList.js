export class PriceList {
  constructor (
    {
      id,
      item,
      price,
      notes,
      discontinued,
      createdAt,
      createdBy,
      modifiedAt,
      modifiedBy
    } = {}
  ) {
    this.id = id
    this.item = item
    this.price = price
    this.notes = notes
    this.discontinued = discontinued
    this.createdAt = createdAt
    this.createdBy = createdBy
    this.modifiedAt = modifiedAt
    this.modifiedBy = modifiedBy
  }
}
