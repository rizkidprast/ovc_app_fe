import UserTracker from './UserTracker'
import { Product } from '../'
export class PurchaseLine extends UserTracker {
  constructor (
    {
      id = 0,
      purchaseId = 0,
      productId = 0,
      unitId = 0,
      qty = 1,
      lineCost = 0,

      // calculated
      // unitCost,

      product = new Product(),

      ...params
    } = {}
  ) {
    super(params)
    this.id = id
    this.purchaseId = purchaseId
    this.productId = productId
    this.unitId = unitId
    this.qty = qty
    this.lineCost = lineCost
    this.product = product

    this.forDeletion = false
  }

  get unitCost () {
    return this.qty > 0 ? this.lineCost / this.qty : 0
  }
}
