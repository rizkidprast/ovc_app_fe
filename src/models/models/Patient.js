import UserTracker from './UserTracker'
import { utility } from '../../boot/utility'
import { AnimalType } from './AnimalType'

export class Patient extends UserTracker {
  constructor (
    {
      id,
      name,
      sex,
      doB,
      animalTypeId,
      clientId,
      animalType,
      ...params
    } = {}
  ) {
    super(params)
    this.id = id
    this.name = name
    this.sex = sex
    this.doB = doB
    this.animalTypeId = animalTypeId
    this.animalType = animalType && animalType !== null ? new AnimalType(animalType) : undefined
    this.clientId = clientId
  }

  get doBString () {
    return this.doB !== undefined ? utility.formatDate(this.doB, 'YYYY/MM/DD') : ''
  }
  set doBString (val) {
    this.doB = utility.extractDate(val, 'YYYY/MM/DD')
  }
}
