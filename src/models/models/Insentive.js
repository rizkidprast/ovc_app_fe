import UserTracker from './UserTracker'
import { User } from './User'
import { SalesLine } from './SalesLine'

export class Insentive extends UserTracker {
  constructor ({
    id,
    appUserId,
    salesLineId,
    value,
    user,
    salesLine,
    ...params
  } = {}) {
    super(params)
    this.id = id
    this.appUserId = appUserId
    this.salesLineId = salesLineId
    this.value = value
    this.user = new User(user)
    this.salesLine = new SalesLine(salesLine)
  }
}
