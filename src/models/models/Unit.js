export class Unit {
  constructor ({ id = 0, code, description } = {}) {
    this.id = id
    this.code = code
    this.description = description
  }
}
