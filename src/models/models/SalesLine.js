import UserTracker from './UserTracker'
import { SalesLineRelatedPic } from './SalesLineRelatedPic'
import { Patient } from './Patient'

export class SalesLine extends UserTracker {
  constructor (
    {
      id,
      salesId = 0,
      productId = null,
      itemName,
      unitId,
      unitCost = 0,
      unitPrice = 0,
      qty = 0,
      discountPercent = 0,
      discount = 0,

      patientId,
      // calculated
      // lineTotal,
      // lineCostTotal,

      salesLineRelatedPics = [],
      sales,
      product,
      patient,
      ...params
    } = {}
  ) {
    super(params)
    this.id = id
    this.itemName = itemName
    this.unitId = unitId
    this.unitCost = unitCost
    this.unitPrice = unitPrice
    this.qty = qty
    this.discountPercent = discountPercent
    this.discount = discount
    this.salesId = salesId
    this.productId = productId
    // this.lineTotal = lineTotal
    // this.lineCostTotal = lineCostTotal

    this.patientId = patientId
    this.salesLineRelatedPics = salesLineRelatedPics.map(x => new SalesLineRelatedPic(x))
    this.sales = sales
    this.product = product
    this.patient = new Patient(patient !== null ? patient : undefined)
    // private
    this.discountUnitInPercent = false
  }

  get totalPriceNoDisc () {
    return this.unitPrice * this.qty
  }
  get totalPrice () {
    return this.totalPriceNoDisc - this.discount - (this.totalPriceNoDisc * this.discountPercent / 100)
  }

  get lineTotal () {
    return this.totalPrice
  }

  get lineCostTotal () {
    return this.unitCost * this.qty
  }

  get hasDiscount () {
    return this.discountPercent > 0 || this.discount > 0
  }
}
