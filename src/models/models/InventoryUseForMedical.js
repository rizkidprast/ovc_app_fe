import UserTracker from './UserTracker'

export class InventoryUseForMedical extends UserTracker {
  constructor ({
    id,
    clientId,
    productId,
    unitId,
    unitCost = 0,

    unitPrice = 0,
    qty = 1,
    prescribedAt = new Date(),
    productName,
    unitName,
    ...params
  } = {}) {
    super(params)
    this.id = id
    this.clientId = clientId
    this.productId = productId
    this.unitId = unitId
    this.unitCost = unitCost
    this.prescribedAt = prescribedAt
    this.productName = productName
    this.unitName = unitName
    this.unitPrice = unitPrice
    this.qty = qty
  }

  get lineTotal () {
    return Number(this.unitPrice) * this.qty
  }
}
