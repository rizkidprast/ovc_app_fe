import UserTracker from './UserTracker'

export class Client extends UserTracker {
  constructor (
    { id = 0,
      code,
      name,
      address,
      phone,
      email,
      note,
      pets,
      totalSpend = 0,

      patients = [],
      ...params } = {}
  ) {
    super(params)
    this.id = id
    this.code = code
    this.name = name
    this.address = address
    this.phone = phone
    this.email = email
    this.note = note
    this.pets = pets
    this.totalSpend = totalSpend

    this.patients = patients
  }
}
