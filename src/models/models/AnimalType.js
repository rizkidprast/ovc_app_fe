export class AnimalType {
  constructor ({ id = 0, code, name } = {}) {
    this.id = id
    this.code = code
    this.name = name
  }
}
