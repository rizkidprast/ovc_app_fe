import UserTracker from './UserTracker'
import { InsentiveRule } from './InsentiveRule'

export class InsentiveModel extends UserTracker {
  constructor ({
    id,
    name,
    description,
    insentiveRules = [],
    ...params
  } = {}) {
    super(params)
    this.id = id
    this.name = name
    this.description = description
    this.insentiveRules = !insentiveRules ? [] : insentiveRules.map(x => new InsentiveRule(x))
  }
}
