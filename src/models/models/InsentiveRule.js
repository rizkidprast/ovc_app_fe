import UserTracker from './UserTracker'

export class InsentiveRule extends UserTracker {
  constructor ({
    id = 0,
    insentiveModelId,
    userTypeId,
    insentiveType,
    value = 0,

    forDeletion = false,
    ...params
  } = {}) {
    super(params)
    this.id = id
    this.insentiveModelId = insentiveModelId
    this.userTypeId = userTypeId
    this.insentiveType = insentiveType
    this.value = value

    this.forDeletion = forDeletion
  }
}
