
const routes = [
  {
    path: '/',
    component: () => import('pages/Login.vue'),
    meta: {
      allowAnonimous: 1
    }
  },
  {
    path: '/login',
    component: () => import('pages/Login.vue'),
    meta: {
      allowAnonymous: 1
    }
  },
  {
    path: '/printSales',
    component: () => import('components/sales/PrintSales'),
    name: 'printSales',
    props: true
  },
  {
    path: '/printDeposit',
    component: () => import('components/sales/printDeposit'),
    name: 'printDeposit',
    props: true
  },
  {
    path: '/frontdesk',
    component: () => import('layouts/FrontDeskLayout'),
    children: [
      {
        path: '', name: 'frontdesk', component: () => import('pages/FrontDeskIndex')
      },
      {
        path: 'sales-today', component: () => import('pages/sales/SalesAndDepositToday')
      },
      {
        path: 'daily-revenues', component: () => import('pages/reports/DailyRevenue')
      },
      {
        path: 'sales-list', component: () => import('pages/sales/SalesAndDepositList')
      },
      {
        path: 'clients', component: () => import('pages/frontdesk/Clients')
      },
      {
        path: 'clients/:id', name: 'frontdesk.clients', component: () => import('pages/frontdesk/Client'), props: true
      }
    ]
  },
  {
    path: '/admin',
    component: () => import('layouts/AdminLayout.vue'),
    children: [
      {
        path: '', component: () => import('pages/AdminIndex.vue')
      },
      {
        path: 'test', component: () => import('pages/HomeIndex.vue')
      },
      {
        path: 'animalTypes', component: () => import('pages/animal-types/animalTypes') },
      {
        path: 'animaltype', name: 'admin.animaltype', component: () => import('pages/animal-types/AnimalType'), props: true
      },
      {
        path: 'categories', component: () => import('pages/categories/Categories') },
      {
        path: 'category', name: 'admin.category', component: () => import('pages/categories/Category'), props: true
      },
      {
        path: 'units', component: () => import('pages/units/Units') },
      {
        path: 'unit', name: 'admin.unit', component: () => import('pages/units/Unit'), props: true
      },
      {
        path: 'products', component: () => import('pages/inventories/Products')
      },
      {
        path: 'product', name: 'admin.product', component: () => import('pages/inventories/Product'), props: true
      },
      {
        path: 'stockOpname', component: () => import('pages/inventories/StockOpnames')
      },
      {
        path: 'calendar', name: 'admin.calendar', component: () => import('pages/appointments/AppointmentCalendar'), props: true
      },
      {
        path: 'clients', component: () => import('pages/clients/Clients') },
      {
        path: 'clients/:id', name: 'admin.clients', component: () => import('pages/clients/ClientDetails'), props: true
      },
      {
        path: 'sales-today', component: () => import('pages/sales/SalesAndDepositToday')
      },
      {
        path: 'sales-list', component: () => import('pages/sales/SalesAndDepositList')
      },
      {
        path: 'purchases', component: () => import('pages/purchases/Purchases')
      },
      {
        path: 'purchase/:id?', name: 'admin.purchase', component: () => import('pages/purchases/Purchase'), props: true
      },
      {
        path: 'insentiveModels', name: 'admin.insentiveModels', component: () => import('pages/insentiveModels/InsentiveModels'), props: true
      },
      {
        path: 'insentiveModel/:id?', name: 'admin.insentiveModel', component: () => import('pages/insentiveModels/InsentiveModel'), props: true
      },

      {
        path: 'users', name: 'admin.users', component: () => import('pages/users/Users')
      },
      {
        path: 'userdetails/:id?', name: 'admin.userdetails', component: () => import('pages/users/UserDetails'), props: true
      },
      {
        path: 'reports/insentives', name: 'admin.reports.insentives', component: () => import('pages/reports/Insentives')
      },
      {
        path: 'reports/sales', name: 'admin.reports.sales', component: () => import('pages/reports/sales')
      },
      {
        path: 'reports/daily-revenues', name: 'admin.reports.dailyRevenue', component: () => import('pages/reports/DailyRevenue')
      },
      {
        path: 'reports/product-transaction-history', name: 'admin.reports.productTransactionHistory', component: () => import('pages/reports/ProductTransactionHistory')
      }
    ]
  }
]

// Always leave this as last one
if (process.env.MODE !== 'ssr') {
  routes.push({
    path: '*',
    component: () => import('pages/Error404.vue')
  })
}

export default routes
