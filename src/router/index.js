import Vue from 'vue'
import VueRouter from 'vue-router'

import routes from './routes'
import store from '../store'

Vue.use(VueRouter)

/*
 * If not building with SSR mode, you can
 * directly export the Router instantiation;
 *
 * The function below can be async too; either use
 * async/await or return a Promise which resolves
 * with the Router instance.
 */

export default function (/* { store, ssrContext } */) {
  const Router = new VueRouter({
    scrollBehavior: () => ({ x: 0, y: 0 }),
    routes,

    // Leave these as they are and change in quasar.conf.js instead!
    // quasar.conf.js -> build -> vueRouterMode
    // quasar.conf.js -> build -> publicPath
    mode: process.env.VUE_ROUTER_MODE,
    base: process.env.VUE_ROUTER_BASE
  })

  Router.beforeEach((to, from, next) => {
    to.matched.some(async (record) => {
      // handle page reload
      let isLogin = store.getters['auth/isLogin']
      if (!isLogin) {
        // console.log('rechecking auth')
        await store.dispatch('auth/getAuth')
        isLogin = store.getters['auth/isLogin']
      }
      if (isLogin && (to.path === '/login' || to.path === '/')) {
        next({
          path: '/admin',
          replace: true
        })
        return
      }

      if (!(record.meta && record.meta.allowAnonymous)) {
        if (!isLogin && to.path !== '/login') {
          next({
            path: '/login',
            replace: true
          })
        }
      }
    })
    next()
  })

  return Router
}
