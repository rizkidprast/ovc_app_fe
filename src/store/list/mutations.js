import { Unit, Category, AnimalType, InsentiveModel, Patient } from '../../models'

export function setAnimalTypes (state, model) {
  if (Array.isArray(model)) {
    const t = model.map(x => new AnimalType(x))
    state.animalTypes = t
  }
}

export function setUnits (state, model) {
  if (Array.isArray(model)) {
    const t = model.map(x => new Unit(x))
    state.units = t
  }
}

export function setCategories (state, model) {
  if (Array.isArray(model)) {
    const t = model.map(x => new Category(x))
    state.categories = t
  }
}

export function setInsentiveModels (state, model) {
  if (Array.isArray(model)) {
    const t = model.map(x => new InsentiveModel(x))
    state.insentiveModels = t
  }
}

export function setPatientsByClient (state, model) {
  if (Array.isArray(model)) {
    var obj = {}
    model.forEach((elm, i) => {
      if (!obj[elm.clientId]) {
        obj[elm.clientId] = []
      }
      obj[elm.clientId].push(new Patient(elm))
    })

    var cState = JSON.parse(JSON.stringify(state.patientsByClient))
    for (var i in obj) {
      console.log(i)
      cState[i] = obj[i]
    }
    state.patientsByClient = cState
  }
}
