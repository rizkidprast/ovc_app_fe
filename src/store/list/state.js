import { UserType } from 'src/models'

export default {
  groups: [],
  roles: [],
  animalTypes: [],
  userTypes: [
    new UserType({
      id: 1,
      name: 'Vet'
    }),
    new UserType({
      id: 2,
      name: 'Vet Assistant'
    }),
    new UserType({
      id: 3,
      name: 'Groomer'
    }),
    new UserType({
      id: 4,
      name: 'Finance'
    }),
    new UserType({
      id: 5,
      name: 'Front desk'
    })
  ],
  appointmentTypes: [
    {
      id: 1,
      name: 'Check up',
      color: 'green'
    },
    {
      id: 2,
      name: 'Vaccination',
      color: 'blue'
    },
    {
      id: 3,
      name: 'Surgery',
      color: '#8e83e6'
    },
    {
      id: 4,
      name: 'Grooming',
      color: 'yellow'
    },
    {
      id: 5,
      name: 'Boarding',
      color: 'purple'
    },
    {
      id: 6,
      name: 'Other',
      color: 'grey'
    }
  ],
  edcTypes: [
    // { value: 0, label: 'NA' },
    { value: 'BCA', label: 'BCA', id: 1 },
    { value: 'MANDIRI', label: 'MANDIRI', id: 2 },
    { value: 'BNI', label: 'BNI', id: 3 }
  ],
  insentiveTypes: [
    { label: 'PerQuantity', value: 'PerQuantity', id: 1 },
    { label: 'PerSales', value: 'PerSales', id: 2 }
  ],
  units: [],
  categories: [],
  insentiveModels: [],
  patientsByClient: {}
}
