export default function () {
  return {
    name: 'Opet Vet Clinic',
    address: 'Jl. Raya Gandul No. 24, Gandul, Cinere, Depok',
    phone: '0812.1910.9119',
    email: '',
    logo: '/statics/logo_opet.png'

  }
}
