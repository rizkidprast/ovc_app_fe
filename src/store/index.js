import Vue from 'vue'
import Vuex from 'vuex'

import auth from './auth'
import office from './office'
import list from './list'

Vue.use(Vuex)

/*
 * If not building with SSR mode, you can
 * directly export the Store instantiation;
 *
 * The function below can be async too; either use
 * async/await or return a Promise which resolves
 * with the Store instance.
 */

// export default function (/* { ssrContext } */) {
export default new Vuex.Store({
  modules: {
    auth,
    office,
    list
  },
  state: {
    errors: [],
    showAppointmentListAt: { atMonth: new Date(), atDate: undefined },
    appointmentId: 0
  },
  actions: {
    addErrors (context, model) {
      context.commit('addErrors', model)
      setTimeout(() => {
        context.commit('clearErrors')
      }, 5000)
    }
  },
  mutations: {
    // setId (state, id) {
    //   state.appointmentId = id
    // },
    setAppointmentListAt (state, showAt) {
      // // console.log('setAppointmentListAt', showAt)
      state.showAppointmentListAt = Object.assign({}, showAt)
    },
    setAppointmentId (state, id) {
      state.appointmentId = id
    },
    clearErrors (state) {
      state.errors = []
    },
    addErrors (state, err) {
      if (err) {
        if (typeof (err) === 'string') {
          state.errors.push(err)
        } else if (Array.isArray(err)) {
          state.errors = Object.assign([], state.errors)
          err.forEach(elm => {
            state.errors.push(elm)
          })
        }
      }
    }
  },

  // enable strict mode (adds overhead!)
  // for dev mode only
  strict: process.env.DEV
})
// return Store
// }
