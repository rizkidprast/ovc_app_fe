import { api } from '../../boot/api'

export const login = async (context, model) => {
  try {
    const res = await api.auth.login(model.userName, model.password)
    context.commit('login', res.data)
    return res
  } catch (e) {
    context.commit('error', e)
    return e
  }
}

export const logout = async (context, model) => {
  try {
    const res = await api.auth.logout()
    context.commit('logout')
    return res
  } catch (e) {
    context.commit('error', e)
    return e
  }
}

export const getAuth = async (context) => {
  try {
    const res = await api.auth.get()
    console.log('reauth', res.data)
    context.commit('login', res.data)
    return res
  } catch (e) {
    // console.log(e);
    context.commit('error', e)
    return e
  }
}
