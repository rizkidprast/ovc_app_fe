export function isLogin (state) {
  return state.userName && state.userName.trim().length > 0
}

export function isAdmin (state) {
  return state.roles.indexOf('Admin') > -1
}

export function isFinance (state) {
  return state.roles.indexOf('Finance') > -1
}

export function isVet (state) {
  return state.roles.indexOf('Veterinarian') > -1
}
