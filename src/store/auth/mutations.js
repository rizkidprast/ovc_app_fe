
import { utility } from '../../boot/utility'

export const login = (state, model) => {
  state = utility.extend(true, state, model)
  state.roles = model.roles.map(x => x)
  state.errors = ''
}

export const logout = (state) => {
  state.userName = ''
  state.displayName = ''
  state.userType = ''
  state.roles = []

  state.errors = ''
}
export const error = (state, e) => {
  if (!e) {
    state.errors = ''
  } else {
    state.errors = utility.err(e)
  }
}
